# Qconsf2019
QConSF 2019

# DAY 1

## Expand on
* vector clock
* normalization
* crdt
* DAGs are Good, Vector clock for versioning.
* Pat Helland AMA

## Yandex Lunch Chat
* sentiment is with restructuring management structure
* building trust, respect, influence Engineer to Developer or Architect to Developer.
* being disconnected from the ground-forces when playing up in the clouds, builds distaste, unsatisfactory job conditions.

## Automated Testing for Terraform, docker, packer, kubernetes, and More - Jim aka Yevgeniy Brikman from Gruntwork 
* write tests for terraform using Go, and Terratest.
* SKIP_<stageName> will skip it from running
* Unit tests -> test module
* Integration tests -> modules + proxy app 
* End-To-End -> should be deployed incrementally rather than all at once. Meaning you need an environment for End-To-End testing, this can also enable behavior tests that show how your consumers wil lbe affected if an update is being pushes. 
* [slides](https://www.slideshare.net/brikis98/how-to-test-infrastructure-code-automated-testing-for-terraform-kubernetes-docker-packer-and-more)


## AWS CDK
The speaker was about to pass out, was sweaty and I almost lost it by trying to hold in my laughter when someone kept sending me textx like, "Im waiting for him to pass out", and "Or maybe a bypass" in reply to "I think he needs some pepto bismal." With that aside, AWS CDK is basically a programmatic tool/way to manage and create CloudFormation templates that follows a different paradigm than Terraform because it allows you to loosely define what you want and it gives it to you, where you do now need to provide all of the inputs to a given module. Aka, it has more defaults. 

## THe talk on parsing JSON fast WOW
An academic and his team Daniel Lemiere [@lemire](https://twitter.com/lemire), decided to use SIMD instructions to parse JSON, which includes checking for valid json, handling unicode and ascii. The beautiful part of the talk is learning about processor branch prediction and its impact on compute cycles used by the CPU and lemire's intent on preventing the porcessor from doing ANY branch prediction by replacing all branching code/conditionals with neat low level bitwise operations and masks. Beautifully done to, 7x local improvements, but an overall parsing of JSON from 0.6GB/s -> 2.5 GB/s .

# DAY 2

## Pneumatic Tube Keynote
Amazingly, a researcher obsessed with pneumatic tubes usage in history ended up detailing the challenges and discoveries of using these tubes to pass messages back in the 19th and 20th century. Starting from the postal service in Paris to the NY post. THe innovations of the time were inspiring to thinking outside the box, seeing the challenges and how they were handled back in the day by brilliant minds of their day.


## HashiCorp Invasion
While hashicorp visited the company that I worked at, I ended up skipping lunch and a session at QCon to go visit HashiCorp's SF office and join the online meeting with our companies HashiCorp reps but from their own offices. :D

Learned about Vault, Consul and Nomad. Gathered enough ammo to go conveniently back to the people those account managers should worry about Daniel Bryant and Matt Turner.

## Daniel and Matt aka Consul and Istio
Right after my meeting with HashiCorp I ran back to QCon because the schedule had an AMA with Matt Turner [@mt165](https://twitter.com/mt165) and Daniel Bryan [@danielbryantuk]([200~https://twitter.com/danielbryantuk) who've been heavily involved in the Service Mesh world, they could provide great insight. During the AMA a ton of topics were brought up, but only a 20% of the overall proposed topics were covered. But I ended up getting my answer, Consul vs Istio. I wont say which. ;)

After the AMA I was forwarded to Gareth Rushgrove and LinkerD by Daniel and i Got a [cool photo](https://twitter.com/MarioWolfe/status/1194398203338084352) of Matt Turner.

* Go and GRPC are the wave
* Stateful Services on kubernetes is hard or bad

## WebAssembly Compiler Build Your Own
This was a little eye opening in terms of lack of contributors. Wasm seems to have been out long enough but it was mentioned that it is new? The talk showed how to create your own compiler, the syntax parsing and transcoding into wasm opcodes. It was alright, did not get too much out of this though but was very pleased seeing someone enjoying lower level dev.

## Evolution of Service Architectures
Talked about progression from Services to Microservices, had the idealness def of microservice where a database is housed with each microservice which implies stateful service, and liked event driven archs using kafka. This is all great and dandy, but we're on k8s, which goes against this paradigm/arch.
* Stateful services on kubernetes is rough. Reinforced from Daniel and Matt before
* Even driven is my favorite, but k8s doesnt really allow it with the benefits of State. Persistant storage is rough with k8s.
* Made me question our arch, which is great, because we used to be all Hadoopy with Cloudera using Kafka, we used to be a streaming arch/ event arch.
* Event sourcing?...

## PPL Probabilistic Programming THE FUTURE ML Engineer
Facebook CS and Stats guy ends up talking about PPL which can be used in writing expressions that statisticians write but programmers need to always work hard to create. I dont know if this is even going to be possible given my limited time doing data science work, but he says it is being used at Facebook to handle detection of fake accounts, supplementing its network of humans that identify if an account is fake or not.
* PPL could possibly be a new space in the industry.
* Nice hair Michael Tingley

# DAY 3

## Culutre Keynote
Every company has a unique culture, and potential employees can always look at company principles online to see if they would grow in said company.

## Ivan's iOS exploits
Talk about dumping apps from memory and static / dynamic analysis on the application and things u should not do.
* Do not store configs that are sensitive in ur app, they are dumped in plaintext, no AWS keys guys
* update ur 3rd party deps
It was simply a compilation of the most common vulns on iOS in addition with how to extract the app fromthe phone for analysis. Even though he 
may have been re-usingo ther peoples discoveries without credit, Ivan is genuine on spreading the info and has his own writeups and courses for free on github. He's real.

## Pole from Tozny on Encrypting using WebAssembly
 The guy basically lied, baited me with the idea of showing me encryption using wasm, waited till the end and he just defined Wasm and WebCrypto, no implementation
 * Voted Yellow because the guy talked but did not bring any real hands on stuff to the presentation.
 * Got baited into staying for wasm, but didnt really show anything.

## Lunch with Boston Devs
They worked at a company that uses kafka and generates insights for customers, alerts customers of peoples behavior to help target marketting campaigns.
Kiyant? I cannot remember the name. 

The guys were funny, meme-ing and let me sit with them at the lunch table. We related well.

## Optimizing yourself Panel SOFT SKILL decisions made to be awesome.
This panel is to talk about people's experiences. We got Daniel Bryant in here ready to speak. WHat to do in career to go up! 
* Community Involvement Contribution
* Personal Core Values align with Business Core Values?
* Do not judge yourself because your work process differs from others
* Dont shy from failure
* Network, ask questions
* Contribute to open source, community.
* Dont stop learning, Resume Driven Development
* Even expert teams have issues or shortcommings.
* Seeing customers reaction and feedback motivates, core values
* Startup benefits: Many responsibilities, frequest deploys, customer feedback, validation for doing job.

## ML in the Browser with Tensorflow.js 
Checked the guy Victor Dibia's Twitter, he has the link for his website that doesnt exist in his twitter profile. Anyone can register the domain and host some NSFW image on it under a proxy registrant. THis would cause our boy Victor some issues in the future. KEK BUR 

FUdge it, I'm leaving. GOing to Ashley Williams talk since shes a RUST core dev working at Cloudflare, might as well get 1 EDGE talk in. ALready been to an ML talk with Probabilistic Programming Languages PPL.

* ditched for another talk

## Pick your Region: Earth, Cloudflare Workers
Ashley WIlliams asks us if we know how the internet works, we all say no.
* Made a good joke comparing Cloudflare to Soundcloud b/c of the big orange cloud logo.
* Really cool person. I think she's wearing a jamaican beanie and curses in her tweets.
* Kenton Varda Fine-Grained Sandbox
* Kenton found out how to use v8:isolate's to run peoples apps which removed a huge chunk of overhead. Amazing REALLY COOL
* Cloudflare removes all local timers so that timing analysis attacks are defunct
* Rust can transpile into webassembly, and run inside of a v8:isolate O_O
* "THe memes will continue until moral improves" - Ashley
* Rust devs are called Crustaceans, Wrangler is the crab for it.
* Cloudflare provides free workers under the .workers.dev domain.
* CDN is fine too, but for static pages u have the ability to do Edge deploys.
* we may be able to blur lines between static and dynamic sites with html rewriters.
* dead easy to deploy sites with github. Free tier exists.
* Wrangler dox built in hugo.
* Chrome extension Lighthouse is great for benchmarking websites. THis tool makes the sites 90 and above out of 100

## SOft SKill
* Find things to do when u have downtime, ask PO
* People are unique, dont communicate to everyone using the same approach, feel people out.
* Feedback needs to be given to ease stresses and keep people on track. The BIO method is great for that
* BIO : Behavior, Impact, Options -> Talking to Maria you called out John, John heard is hurt and slows his work, Please address John 1-on-1 instead.

# Takewaways
* GO and GRPC are the wave
* Consul > Istio , but LinkerD is for the people
* No time for the bullsh1t


# ALL talks attended
See [full schedule](https://qconsf.com/schedule/sf2019/tabular)
*  Speaker AMA w/ Pat Helland Pat Helland - Salesforce 
*   Helm 3: A Mariner's Delight Lachlan Evenson - Microsoft
*    Automated Testing for Terraform, Docker, Packer, Kubernetes, and More Yevgeniy Brikman - Gruntwork
*     AWS Cloud Development Kit (CDK) Richard Boyd - Amazon Web Services
*      Parsing JSON Really Quickly: Lessons Learned Daniel Lemire - University of Quebec
*      It Really is a Series of Tubes Molly Wright Steenson AI, Ethics & Design: Author, Designer, Professor, Research Leader 
*       Kubernetes AMA w/ Daniel Bryant & Matt Turner Daniel Bryant - InfoQ, Matt Turner - Ziglu
*        Build Your Own WebAssembly Compiler Colin Eberhardt - Scott Logic
*         Beyond Microservices: Streams, State and Scalability Gwen Shapira - Confluent
*          Probabilistic programming for software engineers Michael Tingley - Facebook
*           Exploiting Common iOS Apps’ Vulnerabilities Ivan Rodriguez - Google 
*
